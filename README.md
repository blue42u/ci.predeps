# CI Dependencies in .pre

Tired of searching [Docker Hub] for that that one image that has all the packages you need? Struggling to manage multiple versions of custom containers for different Git branches of your project? Want to stop wasting time installing OS packages in all your CI jobs?

Enter `ci.predeps`, a [GitLab CI component] to maintain project/branch/MR/commit-specific containers!

*[Read the latest practical demo and writeup!](https://gitlab.com/blue42u/ci.predeps/-/blob/d8153d54862dc98b7f1a64d10e4899748390df6f/README.md)*

### Features

- Builds from Containerfile(s), committed straight to your project's Git repository!
- Caches and reuses fully-built images, heavily optimized cache hit path!
- Supports shared layer cache for accelerated partial-image updates!
- Builds OCI-compatible images, using the open-source [Buildah]!

## Usage

Add a build script for the container image you want to use in your CI job(s), and commit it to your repository:

```dockerfile
# .ci.predeps/Containerfile
FROM docker.io/alpine
RUN apk add git
```

If you need any other files/directories, add a `.ci.predeps/ignore` file and explicitly un-ignore them:

```gitignore
# .ci.predeps/ignore
**
!ci/
```

Then add the following to your `.gitlab-ci.yml`:

```yaml
include:
- component: gitlab.com/blue42u/ci.predeps/buildah@1

# ...

my-job:
  image: $PREDEPS_IMAGE
  script:
  - git describe --always
```

Example use cases are listed in the [`examples/`](/examples/).
Also see the [available version syntaxes for components](https://docs.gitlab.com/ee/ci/components/#component-versions).

## Contributing

Run into problems? Got a question? Want to request a feature? Feel free to [open an issue](https://gitlab.com/blue42u/ci.predeps/-/issues/new)!

Want to contribute code? Feel free to [fork the project](https://gitlab.com/blue42u/ci.predeps/-/forks/new) and [contribute an MR](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/new)!

## License

This repository follows the [REUSE specification](https://reuse.software/).

All code is under the permissive MIT license. All examples are in the public domain.

[buildah]: https://buildah.io/
[docker hub]: https://hub.docker.com/
[gitlab ci component]: https://docs.gitlab.com/ee/ci/components/
