## 1.0.2 - 2024-09-13

### Fixed

- Fix cascading error when image push fails every retry ([#16](https://gitlab.com/blue42u/ci.predeps/-/issues/16))

## 1.0.1 - 2024-08-03

### Fixed

- Fix errors when using .ignorefiles in a custom context directory ([#15](https://gitlab.com/blue42u/ci.predeps/-/issues/15))

## 1.0.0 - 2024-06-10

*Overriding the generated `predeps: [NAME]` job is no longer supported. Instead, use `job_*` inputs which enforce some basic sanity checks.*

### Changed

- By default, the image build job is `interruptible: false` and `needs: []` ([`1174c60f`])
- By default, the image build job is retried once on any failure ([`2be29546`]). This is configurable via the `retry` input.
- All images have a consistent `--timestamp` value ([`2be29546`]). Previously this only occurred when the layer cache was enabled.

### Added

- New inputs are available for job properties that previously required overlaying the job itself ([!39](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/39))
- Multiple tags can be specified in `job_tags` ([`1174c60f`])

### Fixed

- Required operations are now retried more before failing the job ([#10](https://gitlab.com/blue42u/ci.predeps/-/issues/10))
- Errors arising from network-backed caches are retried ([#11](https://gitlab.com/blue42u/ci.predeps/-/issues/11))

## 0.4.1 - 2024-03-16

### Changed

- Cache mounts are no longer persisted due to performance issues ([#8](https://gitlab.com/blue42u/ci.predeps/-/issues/8))

## 0.4.0 - 2024-03-08

*The main template has been renamed, the new component path is `gitlab.com/blue42u/ci.predeps/buildah@<version>`.*

### Changed

- The build now happens using upstream Buildah images instead of a custom "helper image" ([!26](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/26)). This removes the `helper_image:` input in favor of `buildah_image:` and `skopeo_image:`.
- The template is now named `/buildah` instead of `/build` ([!28](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/28))

### Added

- `build_args:` input can be used to configure build arguments as part of the `inputs:` ([#7](https://gitlab.com/blue42u/ci.predeps/-/issues/7))
- Layer cache is now persisted in the GitLab container registry ([#5](https://gitlab.com/blue42u/ci.predeps/-/issues/5))

### Fixed

- Network operations are now retried on failure ([!21](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/21))

## 0.3.0 - 2024-02-05

*The component path has changed, use `gitlab.com/blue42u/ci.predeps/build@<version>`. The old path is no longer functional on GitLab SaaS.*

### Changed

- Change the component path to work with updates to GitLab ([!20](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/20))

## 0.2.0 - 2023-12-10

### Changed

- **Breaking:** Change builds to happen out of the project directory ([!14](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/14))
- Cut down on CI log output ([#4](https://gitlab.com/blue42u/ci.predeps/-/issues/4))

### Added

- Cache mounts are now persisted ([#1](https://gitlab.com/blue42u/ci.predeps/-/issues/1))
- Allow images with an empty name ([!16](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/16))

## 0.1.2 - 2023-12-07

### Changed

- Reverted to Buildah 1.32.0 to dodge [an upstream regression](https://github.com/containers/buildah/issues/5185) ([!10](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/10)).

## 0.1.1 - 2023-12-05

### Fixed

- Respect `inputs:containerfile`. ([#2](https://gitlab.com/blue42u/ci.predeps/-/issues/2))

## 0.1.0 - 2023-10-21

### Changed

- **Breaking:** `--secret` and `--build-arg` are no longer defined by `secrets:` and `build_args:` inputs.
  Instead, any variables of the form `PREDEPS_SECRET_{id}={path}` become `--secret` flags,
  and any variables of the form `PREDEPS_BUILD_ARG_{name}={value}` become `--build-arg` flags. ([!1](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/1))
- Helper image for improved job performance ([!2](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/2))
- Reuse of upstream prebuilt images ([!3](https://gitlab.com/blue42u/ci.predeps/-/merge_requests/3))

[`1174c60f`]: https://gitlab.com/blue42u/ci.predeps/-/commit/1174c60f06625d8dc679f1965dabe3d12567d56c
[`2be29546`]: https://gitlab.com/blue42u/ci.predeps/-/commit/2be295461343524066ead215193acc42fba9f180
