# SPDX-FileCopyrightText: 2023-2024 Jonathon Anderson <anderson.jonathonm@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

# This is an example Containerfile for using ci.predeps. There are many configuration options
# available for how this image actually gets built, see the examples/ subdirectory.

FROM docker.io/alpine

# Files can be copied in from the project directory, unless they are listed in the ignore file.
# The ignore file is either ".ci.predeps/ignore", ".containerignore" or ".dockerignore", whichever
# is found first.
# If no ignore file is preset, ALL files are ignored by default.
COPY ci/my_file.txt /

# Build arguments can be set by setting a `PREDEPS_BUILD_<name>` environment variable in the
# `predeps: [<name>]` CI job. See the examples/ for more.
ARG MY_ARG="default arg"
ENV ARG_AT_BUILD=$MY_ARG

# Similarly, secrets can be supplied to the build by setting a `PREDEPS_SECRET_<id>` variable.
# The value of this variable is expected to be the path to a file to pull the secret from, as such
# it can also be supplied as a file-type CI variable on GitLab.
# To make manual builds easier, it is recommended to make secrets optional.
RUN --mount=type=secret,id=my_secret \
  ! test -e /run/secrets/my_secret \
  || test "$(tail -n1 /run/secrets/my_secret)" = '!super secret data!'

# All other Docker features supported by Buildah are available, for example cache mounts.
# Note that cache mounts in particular are recommended to reduce network use during the image build,
# but in CI they won't be persisted across CI jobs. Please open an issue at
# https://gitlab.com/blue42u/ci.predeps/-/issues if this feature is of interest to you.
RUN --mount=type=cache,target=/var/cache/apk,sharing=locked \
  apk add valgrind
